FROM php:7.4-fpm

WORKDIR /var/www/webserver

COPY src/ .

COPY test.log /var/www/

EXPOSE 8045
