<?php

error_reporting(0);
try {
    $config = [
        'allowed_methods' => [
            'GET' => true
        ],
        'url_patterns' => [
            '/^\/logs\/(.*)/'
        ],
        'extensions' => [
            'log' => true
        ]
    ];

    if (!array_key_exists($_SERVER['REQUEST_METHOD'],$config['allowed_methods'])) {
        http_response_code('405');
        exit('Method Not Allowed');
    }

    $isAllowedPath = false;
    $matches = null;
    foreach ($config['url_patterns'] as $pattern)
    {
        if (preg_match($pattern, $_SERVER['REQUEST_URI'],$matches)) {
            $isAllowedPath = true;
            break;
        }
    }

    if (!$isAllowedPath) {
        header('HTTP/1.1 404 Not Found');
        exit('Not Found');
    }

    $filename = $matches[1];

    $fileExtension = substr($filename,strripos($filename,'.')+1);

    if (!isset($config['extensions'][$fileExtension])) {
        http_response_code(400);
        exit('Bad Request');
    }

    $pathToFile = '/var/www/'.$filename;
    if (!file_exists($pathToFile)) {
        header('HTTP/1.1 404 Not Found');
        exit('Not Found');
    }

    $fp = fopen($pathToFile,'r');

    if (!$fp) {
        http_response_code(500);
        exit('Server error');
    }

    $totalRespTime = 0;
    while (($buffer = fgets($fp)) !== false) {
        $matches = null;
        preg_match('/resptime:\"(.*)\"/',$buffer,$matches);

        if (strlen($matches[1]) > 0) {
            $nums = explode(', ',$matches[1]);
            $sum = array_reduce($nums, function ($carry,$num) {
                $carry += floatval($num);
                return $carry;
            });

            $totalRespTime += $sum;
        }
    }

    fclose($fp);

    $content = $totalRespTime . ' сек.';

    require_once "page.php";

} catch (\Throwable $e) {
    http_response_code(500);
    exit('Server error');
}